#!/bin/bash
date;
# Cleaning build files.
echo "Cleaning build files...";
cd build;
rm masters_fsm cmake_install.cmake Makefile CMakeCache.txt;
rm -rf CMakeFiles/;
