#include "Commander.h"

Commander::Commander(int team, int ID):
        commander_team(team),
        commander_ID(ID)
{}

Commander::Commander():
        commander_team(-99),
        commander_ID(-99)
{}

void Commander::add_Squad(Squad* member)
{
    member_squads.push_back(member);
    //member->commander_ID = commander_ID;
}

void Commander::calc_Influence(Level &level, vector<vector<int> > &crude_visibility)
{
    // Get list of friendly positions.
    vector<VecXY_Float> friendlies;
    for(unsigned int i = 0; i < member_squads.size(); i++)
    {
        member_squads[i]->get_Agent_Positions(friendlies);
    }

    // Get list of seen enemy positions.
    vector<VecXY_Float> enemies;
    for(unsigned int i = 0; i < member_squads.size(); i++)
    {
        member_squads[i]->get_Seen_Enemies(enemies);
    }

    influence.calcInfluence(friendlies, enemies, level, crude_visibility);
}
