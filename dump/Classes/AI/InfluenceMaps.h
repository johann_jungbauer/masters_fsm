#ifndef INFLUENCE_MAPS_H
#define INFLUENCE_MAPS_H

#include <vector>
#include <cmath>
#include <iostream>
#include <cstdlib>

#include "AI_Agent.h"

#include "../Utils/VecXY_Float.h"

//enum InfMapType
//{
//    FRIENDLY = 0,
//    OPPONENT = 1,
//    GENERAL = 2,
//    TENSION = 3,
//    VULNERABILITY = 4,
//    NO_INF = 5
//};
//
//enum Influence_Type
//{
//    LINEAR = 0,
//    EXPONENTIAL = 1
//};

using namespace std;

class InfluenceMaps
{
    public:
        InfluenceMaps(int width, int height);
        InfluenceMaps();
        void set_Height(int h);
        void set_Width(int w);
        int get_Height();
        int get_Width();
        void setupInfluenceMaps();

        void calcTeamInfluence(vector<AI_Agent*> &agents, Level &level);
        void calcTeamInfluenceV2(vector<AI_Agent*> &agents, Level &level);

        void calcGeneralInfluence();
        void calcTension();
        void calcVulnerability();
//
//        void reCalculateAll(vector<AI_Agent> &allyAgents, vector<AI_Agent> &oppAgents);

        float applyFormula(Influence_Type type, float I0, VecXY_Float sourcePos, int column, int row);

        float infFormula_A(float &I0, VecXY_Float &sourcePos, int &column, int &row);
        float infFormula_B(float &I0, VecXY_Float &sourcePos, int &column, int &row);

        float maximum(float m1, float m2);
        float minimum(float m1, float m2);

        vector<vector<float> > team1;
        vector<vector<float> > team2;
        vector<vector<float> > general;
        vector<vector<float> > tension;
        vector<vector<float> > vulnerability;

        //TODO (general#1#) These should probably have coords attached at some point.
        float team1_max;
        float team1_min;

        float team2_max;
        float team2_min;

        float gen_max;
        float gen_min;

        float ten_max;
        float ten_min;

        float vul_max;
        float vul_min;

        vector<vector<int> > crude_visibility;

    private:
        int _width;
        int _height;
};

#endif
