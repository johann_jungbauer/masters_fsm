#ifndef COMMANDER_H
#define COMMANDER_H

#include <vector>

#include "../Utils/VecXY_Float.h"
#include "../Level.h"

#include "Squad.h"
#include "Command_Influence.h"

class Commander {
	public:
        Commander(int team, int ID);
        Commander();

        int commander_team;
        int commander_ID;
        Command_Influence influence;

        void add_Squad(Squad* member);
        void calc_Influence(Level &level, vector<vector<int> > &crude_visibility);

    private:
        vector<Squad*> member_squads; // Commander is not responsible for the deletion of the objects these pointers point at, Squad + Game are.
};

#endif




