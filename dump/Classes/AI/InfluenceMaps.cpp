#include "InfluenceMaps.h"

InfluenceMaps::InfluenceMaps(int width, int height)
{
    _width = width;
    _height = height;

    setupInfluenceMaps();

    team1_max =0;
    team1_min=0;

    team2_max=0;
    team2_min=0;

    gen_max=0;
    gen_min=0;

    ten_max=0;
    ten_min=0;

    vul_max=0;
    vul_min=0;
}

InfluenceMaps::InfluenceMaps()
{
    team1_max =0;
    team1_min=0;

    team2_max=0;
    team2_min=0;

    gen_max=0;
    gen_min=0;

    ten_max=0;
    ten_min=0;

    vul_max=0;
    vul_min=0;
}

void InfluenceMaps::set_Height(int h)
{
    _height = h;
}

void InfluenceMaps::set_Width(int w)
{
    _width = w;
}

int InfluenceMaps::get_Height()
{
    return _height;
}

int InfluenceMaps::get_Width()
{
    return _width;
}

// Walls DO NOT block influence.
void InfluenceMaps::calcTeamInfluence(vector<AI_Agent*> &agents, Level &level)
{
    float Id=0, currInf=0, inf=0;
    float I0=5.0;
    int grid_x=0, grid_y=0;
    VecXY_Float sourcePos;
    Influence_Type type = EXPONENTIAL;

    /// clear influence
    team1.clear();
    team1.resize(_height, vector<float>(_width, 0));
    team2.clear();
    team2.resize(_height, vector<float>(_width, 0));

    /// apply sources
    for(unsigned int i = 0; i < agents.size(); i++)
    {
        if(agents[i]->info.alive)
        {
            //I0 = allyAgents[i].getPower();
            //TODO (imp#3#) Add influence power to agent info.
            sourcePos = agents[i]->info.current_position;
            level.getGridPositionFromPixels(sourcePos.getX(), sourcePos.getY(), grid_x, grid_y);
            sourcePos.setX(grid_x);
            sourcePos.setY(grid_y);
            //type = allyAgents[i].getInfType();
            //TODO (imp#3#) Add influence type to agent info.

            /// propagate influence
            for(int row = 0; row < _height; row++)
            {
                for(int col = 0; col < _width; col++)
                {
                    Id = applyFormula(type, I0, sourcePos, col, row);

                    if(agents[i]->info.team == 1)
                    {
                        currInf = team1[row][col];
                        inf = currInf + Id;

                        /// set influence
                        team1[row][col] = inf;

                        team1_max = maximum(team1_max, inf);
                        team1_min = minimum(team1_min, inf);
                    }
                    else
                    {
                        currInf = team2[row][col];
                        inf = currInf + Id;

                        /// set influence
                        team2[row][col] = inf;

                        team2_max = maximum(team2_max, inf);
                        team2_min = minimum(team2_min, inf);
                    }
                }
            }
        }
    }

    /// Other map configs
    calcGeneralInfluence();
    calcTension();
    calcVulnerability();
}

// Walls DO block influence.
void InfluenceMaps::calcTeamInfluenceV2(vector<AI_Agent*> &agents, Level &level)
{
    float Id=0, currInf=0, inf=0;
    float I0=5.0;
    int source_grid_x=0, source_grid_y=0, grid_x=0, grid_y=0;
    VecXY_Float sourcePos;
    Influence_Type type = EXPONENTIAL;
    Tile source_tile;
    Tile propagate_tile;

    /// clear influence
    team1.clear();
    team1.resize(_height, vector<float>(_width, 0));
    team2.clear();
    team2.resize(_height, vector<float>(_width, 0));

    /// apply sources
    for(unsigned int i = 0; i < agents.size(); i++)
    {
        if(agents[i]->info.alive)
        {
            //I0 = allyAgents[i].getPower();
            //TODO (imp#3#) Add influence power to agent info.
            sourcePos = agents[i]->info.current_position;
            level.getGridPositionFromPixels(sourcePos.getX(), sourcePos.getY(), source_grid_x, source_grid_y);
            sourcePos.setX(source_grid_x);
            sourcePos.setY(source_grid_y);
            //type = allyAgents[i].getInfType();
            //TODO (imp#3#) Add influence type to agent info.
            level.get_tile(source_grid_x, source_grid_y, source_tile);

            /// propagate influence
            /// only propagate to tiles that can see source tile.
            for(unsigned int p = 0; p < crude_visibility[source_tile.map_ref].size(); p++)
            {
                level.get_tile(crude_visibility[source_tile.map_ref][p], propagate_tile);
                propagate_tile.GetGridPosition(grid_x, grid_y);
                Id = applyFormula(type, I0, sourcePos, grid_x, grid_y);

                if(agents[i]->info.team == 1)
                {
                    currInf = team1[grid_y][grid_x];
                    inf = currInf + Id;

                    /// set influence
                    team1[grid_y][grid_x] = inf;

                    team1_max = maximum(team1_max, inf);
                    team1_min = minimum(team1_min, inf);
                }
                else
                {
                    currInf = team2[grid_y][grid_x];
                    inf = currInf + Id;

                    /// set influence
                    team2[grid_y][grid_x] = inf;

                    team2_max = maximum(team2_max, inf);
                    team2_min = minimum(team2_min, inf);
                }
            }
        }
    }

    /// Other map configs
    calcGeneralInfluence();
    calcTension();
    calcVulnerability();
}

void InfluenceMaps::calcGeneralInfluence()
{
    float inf = 0;

    /// clear influence
    general.clear();
    general.resize(_height, vector<float>(_width, 0));


    for(int row = 0; row < _height; row++)
    {
        for(int col = 0; col < _width; col++)
        {
            inf = team1[row][col] - team2[row][col];
            general[row][col] = inf;

            gen_max = maximum(gen_max, inf);
            gen_min = minimum(gen_min, inf);
        }
    }
}

void InfluenceMaps::calcTension()
{
    float inf = 0;

    /// clear influence
    tension.clear();
    tension.resize(_height, vector<float>(_width, 0));


    for(int row = 0; row < _height; row++)
    {
        for(int col = 0; col < _width; col++)
        {
            inf = team1[row][col] +team2[row][col];
            tension[row][col] = inf;
            ten_max = maximum(ten_max, inf);
            ten_min = minimum(ten_min, inf);
        }
    }
}

void InfluenceMaps::calcVulnerability()
{
    float inf = 0;

    /// clear influence
    vulnerability.clear();
    vulnerability.resize(_height, vector<float>(_width, 0));


    for(int row = 0; row < _height; row++)
    {
        for(int col = 0; col < _width; col++)
        {
            inf = tension[row][col] - abs(general[row][col]);
            vulnerability[row][col] = inf;
            vul_max = maximum(vul_max, inf);
            vul_min = minimum(vul_min, inf);
        }
    }
}

void InfluenceMaps::setupInfluenceMaps()
{
    team1.resize(_height, vector<float>(_width, 0));
    team2.resize(_height, vector<float>(_width, 0));
    general.resize(_height, vector<float>(_width, 0));
    tension.resize(_height, vector<float>(_width, 0));
    vulnerability.resize(_height, vector<float>(_width, 0));
}

//void InfluenceMaps::reCalculateAll(vector<AI_Agent> &allyAgents, vector<AI_Agent> &oppAgents)
//{
//    calcAllyInfluence(allyAgents);
//    calcOppInfluence(oppAgents);
//
//    calcGeneralInfluence();
//    calcTension();
//    calcVulnerability();
//}

float InfluenceMaps::applyFormula(Influence_Type type, float I0, VecXY_Float sourcePos, int column, int row)
{
    float Id = 0;

    switch(type)
    {
        case LINEAR:
        {
            Id = infFormula_A(I0, sourcePos, column, row);
        }
        break;

        case EXPONENTIAL:
        {
            Id = infFormula_B(I0, sourcePos, column, row);
        }
        break;

        default:
        {}

    }

    return Id;
}

float InfluenceMaps::infFormula_A(float &I0, VecXY_Float &sourcePos, int &column, int &row)
{
    float Id=0, xDiff=0, yDiff=0;
    float d=0;

    xDiff = sourcePos.getX() - (float)column;
    yDiff = sourcePos.getY() - (float)row;
    d = sqrt( xDiff*xDiff + yDiff*yDiff);

    Id = I0/(d+1);

    return Id;
}

float InfluenceMaps::infFormula_B(float &I0, VecXY_Float &sourcePos, int &column, int &row)
{
    /// from Game Progamming Gems 2 : Influence Mapping
    float Id=0, xDiff=0, yDiff=0;
    float d=0, fallOff=0, multi=0;

    xDiff = sourcePos.getX() - (float)column;
    yDiff = sourcePos.getY() - (float)row;
    d = sqrt( xDiff*xDiff + yDiff*yDiff);

    fallOff = 0.75;
    multi = pow(fallOff,d);

    Id = I0 * multi;

    return Id;
}

float InfluenceMaps::maximum(float m1, float m2)
{
    if(m1>m2)
    {
        return m1;
    }
    else
    {
        return m2;
    }
}

float InfluenceMaps::minimum(float m1, float m2)
{
    if(m1<m2)
    {
        return m1;
    }
    else
    {
        return m2;
    }
}
